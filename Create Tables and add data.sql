﻿USE [TestDatabase]
GO
/****** Object:  Table [dbo].[Classification]    Script Date: 22/06/2017 09:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Classification](
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Classification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Company]    Script Date: 22/06/2017 09:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Company](
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[ClassificationId] [int] NOT NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 22/06/2017 09:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[DateOfBirth] [datetime] NULL,
	[EmailAddress] [varchar](50) NULL,
	[HasCreditLimit] [bit] NULL,
	[CreditLimit] [int] NULL,
	[CompanyId] [int] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Classification] ([Id], [Name]) VALUES (1, N'Bronze')
GO
INSERT [dbo].[Classification] ([Id], [Name]) VALUES (2, N'Silver')
GO
INSERT [dbo].[Classification] ([Id], [Name]) VALUES (3, N'Gold')
GO
INSERT [dbo].[Company] ([Id], [Name], [ClassificationId]) VALUES (1, N'Filipili Co', 1)
GO
INSERT [dbo].[Company] ([Id], [Name], [ClassificationId]) VALUES (2, N'Genako Pl', 2)
GO
INSERT [dbo].[Company] ([Id], [Name], [ClassificationId]) VALUES (3, N'Jeraldis Tech', 3)
GO
INSERT [dbo].[Company] ([Id], [Name], [ClassificationId]) VALUES (4, N'Lu Chukis Labs', 2)
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

GO
INSERT [dbo].[Customer] ([Id], [Firstname], [Surname], [DateOfBirth], [EmailAddress], [HasCreditLimit], [CreditLimit], [CompanyId]) VALUES (1, N'Phill', N'Late', CAST(N'1987-11-21 00:00:00.000' AS DateTime), N'phil@phil.co', 1, 2100, 1)
GO
INSERT [dbo].[Customer] ([Id], [Firstname], [Surname], [DateOfBirth], [EmailAddress], [HasCreditLimit], [CreditLimit], [CompanyId]) VALUES (2, N'Nancy', N'Pleat', CAST(N'1970-11-20 00:00:00.000' AS DateTime), N'nance@yahoo.com', 1, 2000, 2)
GO
INSERT [dbo].[Customer] ([Id], [Firstname], [Surname], [DateOfBirth], [EmailAddress], [HasCreditLimit], [CreditLimit], [CompanyId]) VALUES (3, N'Feku', N'Nivasaan', CAST(N'1981-10-22 00:00:00.000' AS DateTime), N'r.niv@gmail.com', 1, 1500, 3)
GO
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_Classification] FOREIGN KEY([ClassificationId])
REFERENCES [dbo].[Classification] ([Id])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Company_Classification]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Company]
GO
/****** Object:  StoredProcedure [dbo].[uspAddCustomer]    Script Date: 22/06/2017 09:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[uspAddCustomer] 
	-- Add the parameters for the stored procedure here
	@Firstname varchar(50), 
	@Surname varchar(50), 
	@DateOfBirth datetime, 
	@EmailAddress varchar(50), 
	@HasCreditLimit bit,
	@CreditLimit int,
	@CompanyId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Customer]
           ([Firstname]
           ,[Surname]
           ,[DateOfBirth]
           ,[EmailAddress]
           ,[HasCreditLimit]
           ,[CreditLimit]
           ,[CompanyId])
     VALUES
           (@Firstname
           ,@Surname
           ,@DateOfBirth
           ,@EmailAddress
           ,@HasCreditLimit
           ,@CreditLimit
           ,@CompanyId)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllCustomers]    Script Date: 22/06/2017 09:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllCustomers]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure herex
	SELECT * FROM Customer
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetCompanyById]    Script Date: 22/06/2017 09:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetCompanyById] 
	-- Add the parameters for the stored procedure here
	@CompanyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Company where Id = @CompanyId
END

GO
