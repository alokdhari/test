﻿/* jshint undef: true, unused: true */
/* global  angular */
(function (angular) {
    'use strict';

    var customersmodule = angular.module('customersApp.customers', ['ui.router', 'ui.bootstrap']);

    customersmodule.config([
            '$stateProvider',
            function ($stateProvider) {
                $stateProvider
                    .state("customers",
                    {
                        templateUrl: "/scripts/app/customers/customers.html",
                        url: "/customers",
                        controller: 'customersCtrl'
                    })
                    .state("addcustomer",
                    {
                        templateUrl: "/scripts/app/customers/editCustomer.html",
                        url: "/customer/{id}",
                        controller: 'editCustomerCtrl',
                        resolve: {
                                'customer': ['customerService', '$stateParams',
                                    function (customerService, $stateParams) {
                                        if ($stateParams.id === 'new') {
                                            return customerService.createNew();
                                        }
                                        return customerService
                                            .query({ key: $stateParams.id })
                                            .$promise;
                                    }]
                            }
                    });
            }
    ]);
})(angular);