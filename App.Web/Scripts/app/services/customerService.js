﻿(function (angular) {
    var serviceModule = angular.module('customersApp.services');

    serviceModule.service('customerService', [
            '$resource', 'configuration', function ($resource, configuration)
            {
                var apiUrl = configuration.api.customers;
                var service = $resource("", {}, {
                    'getAll': {
                            method: "GET",
                            url: apiUrl,
                            isArray: true
                        },
                    'query': {
                            method: 'GET',
                            url: apiUrl + "?key=:id",
                            isArray: true
                        },
                    'save': { method: "POST", url: apiUrl }
                });

                service.createNew = function () {
                    var dto = {
                            isNew: true
                        };

                    var cleanseDto = function () {
                        delete dto.isNew;
                    };

                    dto.$save = function (success, error) {
                        cleanseDto();
                        return service.save(null, dto, success, error);
                    };

                    return dto;
                };

                return service;
            }
        ]);
})(angular);