﻿using System;
using System.Collections.Generic;

namespace App.Services
{
    public class CustomerService
    {
        public bool AddCustomer(string firname, string surname, string email, DateTime dateOfBirth, int companyId)
        {
            if (string.IsNullOrEmpty(firname) || string.IsNullOrEmpty(surname))
            {
                return false;
            }

            if (!email.Contains("@") && !email.Contains("."))
            {
                return false;
            }

            var now = DateTime.Now;
            int age = now.Year - dateOfBirth.Year;
            if (now.Month < dateOfBirth.Month || (now.Month == dateOfBirth.Month && now.Day < dateOfBirth.Day)) age--;

            if (age < 21)
            {
                return false;
            }

            var companyRepository = new CompanyRepository();
            var company = companyRepository.GetById(companyId);

            var customer = new Customer
                               {
                                   Company = company,
                                   DateOfBirth = dateOfBirth,
                                   EmailAddress = email,
                                   Firstname = firname,
                                   Surname = surname,
                                   CreditLimit = 1000
            };

            if (company.Classification == Classification.Bronze)
            {
                // Skip credit check
                customer.HasCreditLimit = false;
            }
            else if (company.Classification == Classification.Gold || company.Classification == Classification.Silver)
            {
                // Do credit check and double credit limit
                customer.HasCreditLimit = true;
                if(company.Classification == Classification.Gold)
                {
                    customer.CreditLimit = customer.CreditLimit * 2;
                }
            }
            else
            {
                // Do credit check
                customer.HasCreditLimit = true;
            }

            CustomerDataAccess.AddCustomer(customer);

            return true;
        }

        public List<Customer> GetAll()
        {
            return CustomerDataAccess.GetAllCustomers();
        }
    }
}
