﻿/* jshint undef: true, unused: true */
/* global  angular */
(function (angular) {
    'use strict';

    var homeModule = angular.module('customersApp.home', ['ui.router', 'ui.bootstrap']);

    homeModule.config([
            '$stateProvider',
            function ($stateProvider) {
                $stateProvider.state("home",
                    {
                        templateUrl: "/scripts/app/home/home.html",
                        url: "/home",
                        controller: 'HomeCtrl'
                    });
            }
        ]);

    homeModule.controller('HomeCtrl',
        [
            '$scope',
            function($scope)
            {
            }
        ]);
})(angular);