﻿/* jshint undef: true, unused: true */
/* global  angular */
(function (angular) {
    'use strict';

    var customersmodule = angular.module('customersApp.customers');
    
    customersmodule.controller('editCustomerCtrl',
        [
            '$scope', 'customer', '$stateParams', '$state', '$timeout',
            function ($scope, customer, $stateParams, $state, $timeout)
            {
                var successMessage = {};
                successMessage.Class = 'alert-success';
                successMessage.Heading = 'Success';
                successMessage.Body = 'Saved the customer. Will redirect in 5 seconds.';

                var failureMessage = {};
                failureMessage.Class = 'alert-error';
                failureMessage.Heading = 'Error';
                failureMessage.Body = '';

                $scope.addingCustomer = $stateParams.id === 'new';
                $scope.customer = customer;
                $scope.save = function()
                {
                    if ($scope.customer.isNew)
                    {
                        $scope.customer.Company = {};
                        $scope.customer.Company.Id = $scope.customer.CompanyId;
                        $scope.customer.$save(function(result)
                        {
                            $scope.message = successMessage;
                            $scope.message.showMessage = true;
                            $timeout(function()
                            {
                                $state.go('customers');
                                },
                                5000);
                        }, function(error)
                        {
                            $scope.message = failureMessage;
                            $scope.message.showMessage = true;
                        });
                    }
                    else
                    {
                        $scope.message = failureMessage;
                        $scope.message.Body = 'Edit is not enabled yet.';
                        $scope.message.showMessage = true;
                    }
                };
            }
        ]);
})(angular);