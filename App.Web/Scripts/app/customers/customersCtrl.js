﻿/* jshint undef: true, unused: true */
/* global  angular */
(function (angular) {
    'use strict';

    var customersmodule = angular.module('customersApp.customers');
    
    customersmodule.controller('customersCtrl',
        [
            '$scope', 'customerService',
            function ($scope, customerService)
            {
                $scope.customers = [];
                var result = customerService.getAll(function (result) {
                    $scope.customers = result;
                });
            }
        ]);
})(angular);