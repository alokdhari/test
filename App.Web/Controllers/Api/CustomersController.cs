﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using App.Services;

namespace App.Web.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private CustomerService customerService;

        public CustomersController()
        {
            this.customerService = new CustomerService();
        }

        public IHttpActionResult Get()
        {
            var customers = this.customerService.GetAll();
            return this.Ok(customers);
        }

        public IHttpActionResult Get(int id)
        {
            var customers = this.customerService.GetAll();
            if(customers.Exists(o => o.Id == id))
            {
                return this.Ok(customers.First(o => o.Id == id));
            }
            else
            {
                return this.NotFound();
            }
        }

        public IHttpActionResult Post(Customer customer)
        {
            var created = this.customerService
                .AddCustomer(customer.Firstname,
                             customer.Surname,
                             customer.EmailAddress,
                             customer.DateOfBirth,
                             customer.Company.Id);

            if(created)
            {
                return this.Ok();
            }
            else
            {
                return this.InternalServerError();
            }
        }
    }
}
