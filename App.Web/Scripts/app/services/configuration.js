﻿(function (angular) {
    var serviceModule = angular.module('customersApp.services');

    serviceModule.factory('configuration',
        [
            '$http', 
            function ($http) {
                
                var api = {
                        customers: '/api/Customers'
                    };

                return {
                        api: api
                    };
            }
        ]);
})(angular);