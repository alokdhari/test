﻿/// <vs />
/// <binding ProjectOpened = 'watch:scripts' />
/// <vs BeforeBuild = 'copy' />
module.exports = function (grunt) { // Project configuration.
    grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),
            clean: ["scripts/build/*"],
            bower: {
                    install: {
                            options: {
                                    copy: false
                                }
                        }
                },
            copy: {
                    main: {
                            files: [
                                    { expand: true, src: ['node_modules/angular/*.min.js', 'node_modules/angular/*.min.js.map'], dest: 'Scripts/vendor/angular/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-aria/*.min.*'], dest: 'Scripts/vendor/angular-aria/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-route/*.min.*'], dest: 'Scripts/vendor/angular-route/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-mocks/*.js'], dest: 'Scripts/vendor/angular-mocks/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-material/angular-material-mocks.js'], dest: 'Scripts/vendor/angular-mock/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-local-storage/dist/*.js', 'node_modules/angular-local-storage/dist/*.js.map'], dest: 'Scripts/vendor/angular-local-storage/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-loading-bar/build/*.js'], dest: 'Scripts/vendor/angular-loading-bar/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-loading-bar/build/*.css'], dest: 'content/styles/angular-loading-bar/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-resource/*.min.*'], dest: 'Scripts/vendor/angular-resource/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-ui-router/release/*.js'], dest: 'Scripts/vendor/angular-ui-router/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-ui-bootstrap/dist/*.js'], dest: 'Scripts/vendor/angular-ui-bootstrap/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-ui-bootstrap/dist/*.css'], dest: 'content/styles/angular-ui-bootstrap/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-messages/*.js'], dest: 'Scripts/vendor/angular-messages/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/bootstrap/dist/css/*.*'], dest: 'content/styles/bootstrap/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/bootstrap/dist/fonts/*.*'], dest: 'content/styles/fonts/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/bootstrap/dist/js/*.*'], dest: 'Scripts/vendor/bootstrap/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/bootstrap-material-design/dist/css/*.*'], dest: 'content/styles/bootstrap-material-design/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/bootstrap-material-design/dist/js/*.*'], dest: 'Scripts/vendor/bootstrap-material-design/', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-breadcrumb/dist/*.*'], dest: 'Scripts/vendor/angular-breadcrumb', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/angular-breadcrumb/dist/*.*'], dest: 'Scripts/vendor/angular-breadcrumb', flatten: true, filter: 'isFile' },
                                    { expand: true, src: ['node_modules/jquery/dist/*.js'], dest: 'Scripts/vendor/jquery/', flatten: true, filter: 'isFile' }
                                ]
                        }
                },
            jshint: {
                    all: ['Gruntfile.js', 'scripts/account/**/*.js', 'scripts/app/**/*.js']
                },
            uglify: {
                    options: {
                            sourceMap: true,
                            banner: '/*! Cancer Research UK <%= pkg.name %> - v<%= pkg.version %> - ' +
                                '<%= grunt.template.today("yyyy-mm-dd") %> */',
                            mangle: false,
                            quoteStyle: 1
                        },
                    all: {
                            files: {
                                    'scripts/build/testSpa.js': ['scripts/app/**/*.js']
                                }
                        }
                },
            watch: {
                    scripts: {
                            files: ['scripts/app/**/*.js'],
                            tasks: ['concat'],
                            options: {
                                    livereload: true,
                                    spawn: false
                                }
                        },
                    html: {
                            files: ['scripts/app/**/*.html'],
                            options: {
                                    livereload: true
                                }
                        },
                    css: {
                            files: ['Content/**/*.css'],
                            options: {
                                    livereload: true
                                }
                        }
                },
            ngAnnotate: {
                    options: {
                            singleQuotes: true
                        },
                    all: {
                            files: [
                                    {
                                        expand: true,
                                        cwd: 'scripts/account',
                                        src: '**/*.js',
                                        dest: 'scripts/account'
                                    }
                                ]
                        }
                },
            concat: {
                    options: {
                            stripBanners: true,
                            banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                                '<%= grunt.template.today("yyyy-mm-dd") %> */',
                            separator: '\n'
                        },
                    allFiles: {
                        files: {
                                'scripts/build/testSpa.js': ['scripts/app/**/*.js']
                            }
                        }
                }
        });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-npm-install');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');

    // Default task(s).
    grunt.registerTask('default', ['clean', 'copy', 'concat:allFiles']);
    grunt.registerTask('buildandwatch', ['clean', 'copy', 'concat:allFiles', 'watch']);
};