﻿(function(angular)
{
    var app = angular.module("customersApp", [
        'angular-loading-bar',
        'ui.router',
        'ui.bootstrap',
        'customersApp.home',
        'customersApp.customers',
        'customersApp.services']);


    app.config([
            '$stateProvider', '$urlRouterProvider', '$httpProvider', 'cfpLoadingBarProvider',
            function ($stateProvider, $urlRouterProvider, $httpProvider, cfpLoadingBarProvider) {
                cfpLoadingBarProvider.includeSpinner = false;
                
                $urlRouterProvider.when('/', '/home');

                $urlRouterProvider.otherwise("/");
                if (!$httpProvider.defaults.headers.get) {
                    $httpProvider.defaults.headers.get = {};
                }

                // Answer edited to include suggestions from comments
                // because previous version of code introduced browser-related errors

                $httpProvider.defaults.useXDomain = true;
                $httpProvider.defaults.headers.common['Content-Type'] = "application/x-www-form-urlencoded";
                //disable IE ajax request caching
                $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
                // extra
                $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
                $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
                $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
            }
        ]);

    app.run([
            '$rootScope', '$log', '$http',
            function ($rootScope, $log, $http) {

                $rootScope.rootUIState = {};

                $rootScope.$on('cfpLoadingBar:completed', function (event) {
                    $rootScope.rootUIState.cfpLoadingBarActive = false;
                });
                $rootScope.$on('cfpLoadingBar:started', function (event) {
                    $rootScope.rootUIState.cfpLoadingBarActive = true;
                });

            }
        ]);
})(angular);