﻿(function (angular) {
    var module = angular.module('customersApp.services',
        [
            "ngResource",
            "ui.router"]);
})(angular);